package edu.nvcc.mts.machine;

import edu.nvcc.mts.interpreter.Statement;
import edu.nvcc.mts.container.Pair;

import java.util.List;

public class Machine {
    private byte[] memory = new byte[30];
    private CPU[] cores;

    public Machine(List<Pair<List<Statement>, String>> sources, boolean shared){
        cores = new CPU[sources.size()];
        for(int i = 0; i < cores.length; i++){
            if(shared) {
                cores[i] = new CPU(sources.get(i).first, memory, sources.get(i).second);
            }else{
                cores[i] = new CPU(sources.get(i).first, new byte[30], sources.get(i).second);
            }
        }
    }

    public void execute(){
        Thread[] threads = new Thread[cores.length];

        for(int i = 0; i < cores.length; i++){
            threads[i] = new Thread(cores[i]);
            threads[i].start();
        }

        for(Thread i : threads){
            try {
                i.join();
            }catch(Exception e){
                System.err.println("unknown thread interrupted");
            }
        }
        System.out.println("Program completed");
    }
}
