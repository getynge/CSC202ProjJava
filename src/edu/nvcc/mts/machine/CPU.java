package edu.nvcc.mts.machine;

import edu.nvcc.mts.interpreter.Statement;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class CPU implements Runnable{
    private ArrayDeque<Byte> stack = new ArrayDeque<>();
    private final List<Statement> program;
    private final byte[] memory;
    private final byte[] registers = new byte[5];
    private final String filename;

    CPU(List<Statement> program, byte[] memory, String filename){
        this.memory = memory;
        this.program = program;
        this.filename = filename;
    }

    @Override
    public void run() {
        for(int i = 0; i < program.size(); i++){
            List<Byte> args = program.get(i).getArguments();
            switch(program.get(i).opcode){
                case "add":
                    byte addResult = (byte) (registers[args.get(0)] + registers[args.get(1)]);
                    registers[args.get(0)] = addResult;
                    break;
                case "mult":
                    byte multResult = (byte) (registers[args.get(0)] * registers[args.get(1)]);
                    registers[args.get(0)] = multResult;
                    break;
                case "store":
                    memory[args.get(1)] = registers[args.get(0)];
                    break;
                case "fetch":
                    registers[args.get(0)] = memory[args.get(1)];
                    break;
                case "equ":
                    if(registers[args.get(0)] == registers[args.get(1)]){
                        registers[args.get(2)] = 1;
                    }else{
                        registers[args.get(2)] = 0;
                    }
                    break;
                case "push":
                    stack.push(registers[args.get(0)]);
                    break;
                case "pop":
                    try {
                        registers[args.get(0)] = stack.pop();
                    }catch(NoSuchElementException e){
                        System.err.println(filename + ": thread killed due to attempt to pop off empty stack on line " + i);
                        return;
                    }
                    break;
                case "goto":
                    byte offset = registers[args.get(0)];
                    if(offset == -1){
                        System.err.println(filename + ": thread killed due to unconditional infinite loop on line " + i);
                        return;
                    }
                    if(i + offset < 0){
                        System.err.println(filename + ": thread killed due to illegal goto on line " + i);
                        System.err.println("Attempted to jump by " + offset + " lines");
                        System.err.println("Which would result in illegal line location " + (i + offset));
                        return;
                    }
                    i += offset - 1;
                    break;
                case "load":
                    registers[args.get(0)] = args.get(1);
                    break;
                case "out":
                    System.out.println(registers[args.get(0)]);
                    break;
                case "if":
                    if(registers[args.get(0)] == 1){
                        i += registers[args.get(1)] - 1;
                    }
                    break;
                case "debug":
                    synchronized (System.out) {
                        switch (args.get(0)) {
                            case 1:
                                System.out.print("registers: " + Arrays.toString(registers));
                                System.out.println(" " + i);
                                break;
                            case 2:
                                synchronized (memory) {
                                    System.out.println("memory: " + Arrays.toString(memory));
                                }
                                break;
                            case 3:
                                System.out.println("stack: " + stack.toString());
                                break;
                            case 4:
                                synchronized (memory) {
                                    System.out.print("registers: " + Arrays.toString(registers));
                                    System.out.println(" " + i);
                                    System.out.println("memory: " + Arrays.toString(memory));
                                    System.out.println("stack: " + stack.toString());
                                }
                                break;
                        }
                    }
                    break;
                case "end":
                    return;
                default:
                    System.err.println(filename + ": An unknown error occurred on line " + i);
                    return;
            }
        }
    }
}