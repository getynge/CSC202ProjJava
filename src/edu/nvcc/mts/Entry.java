package edu.nvcc.mts;

import edu.nvcc.mts.interpreter.*;
import edu.nvcc.mts.container.Pair;
import edu.nvcc.mts.machine.Machine;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Entry {
    public static void main(String[] args){
        if(args.length < 1){
            System.err.println("Insufficient arguments, please list at least one file to run");
            return;
        }
        Scanner handler = new Scanner(System.in);
        ArrayList<String> contents = new ArrayList<>();
        ArrayList<Pair<List<Statement>, String>> compiled;
        Machine machine;
        boolean useSharedMemory = false;
        for (String arg : args) {
            if (arg.startsWith("-")) {
                switch (arg.toLowerCase()) {
                    case "-h":
                    case "--help":
                        System.out.println("Executes one or more assembly files in parallel");
                        System.out.println("By default, memory is not shared between programs");
                        System.out.println("\nCommands:");
                        System.out.println("-h --help\tPrints this message");
                        System.out.println("-s --sharedMemory\tShares memory between executables");
                        break;
                    case "-s":
                    case "--sharedmemory":
                        useSharedMemory = true;
                }
            }
            File source = new File(arg);
            if (source.exists() && source.isFile() && source.canRead()) {
                try (BufferedReader fr = new BufferedReader(new FileReader(source))) {
                    StringBuilder builder = new StringBuilder();
                    fr.lines().forEach(x -> builder.append(x).append("\n"));
                    contents.add(builder.toString());
                } catch (FileNotFoundException e) {
                    System.err.println("Error: file that should exist could not be found: " + arg);
                    System.err.println("Type y to continue, hit enter to exit");
                    if (!handler.nextLine().trim().toLowerCase().equals("y")) {
                        return;
                    }
                } catch (IOException e) {
                    System.err.println("There was an error reading file " + arg);
                    System.err.println("Type y to continue, hit enter to exit");
                    if (!handler.nextLine().trim().toLowerCase().equals("y")) {
                        return;
                    }
                }
            } else {
                System.err.println("Nonfatal error: the file " + arg + " cannot be read");
                System.err.println("Continuing to next file");
            }
        }

        if(contents.size() < 1){
            System.err.println("Insufficient arguments, at least one argument must be a valid file");
            return;
        }
        if(contents.size() > 4){
            System.err.println("Too many files, only four files can be read at a time");
            return;
        }
        compiled = new ArrayList<>();


        for (String content : contents) {
            try {
                Lexer l = new Lexer(content);
                Parser p = new Parser(l.lex());
                compiled.add(new Pair<>(p.parse(), content));
            } catch (LexingException e) {
                System.err.println("There was an error lexing the file on line " + e.line);

                System.err.println(e.getMessage());
                System.err.println("\nNote: The above line count does not include comments or empty lines");
                System.err.println("Type y to continue, hit enter to print the stack trace and exit");
                if (!handler.nextLine().trim().toLowerCase().equals("y")) {
                    e.printStackTrace();
                    return;
                }
            } catch (ParsingException e) {
                System.err.println("There was an error parsing the file on line " + e.line);
                System.err.println(e.getMessage());
                System.err.println("\nNote: The above line count does not include comments or empty lines");
                System.err.println("Type y to continue, hit enter to print the stack trace and exit");
                if (!handler.nextLine().trim().toLowerCase().equals("y")) {
                    e.printStackTrace();
                    return;
                }
            } catch (Exception e) {
                System.err.println("An unknown error occurred with the following message");
                System.err.println(e.getMessage());
                System.err.println("Type y to continue, hit enter to print the stack trace and exit");
                if (!handler.nextLine().trim().toLowerCase().equals("y")) {
                    e.printStackTrace();
                    return;
                }
            }
        }
        machine = new Machine(compiled, useSharedMemory);

        machine.execute();
    }
}
