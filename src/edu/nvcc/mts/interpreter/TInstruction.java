package edu.nvcc.mts.interpreter;

public class TInstruction implements Token {
    private final String ID;

    @Override
    public boolean innerMatch(Token other) {
        if(outerMatch(other)){
            TInstruction o = (TInstruction) other;
            return o.ID.equals(ID);
        }
        return false;

    }

    TInstruction(String id){
        ID = id;
    }

    @Override
    public String toString() {
        return ID;
    }
}
