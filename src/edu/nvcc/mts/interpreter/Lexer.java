package edu.nvcc.mts.interpreter;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
    private final String text;
    private boolean lexed = false;
    private ArrayList<Token> list = new ArrayList<>();

    public Lexer(String text){
        this.text = text.toLowerCase();

    }

    private boolean isByte(String m){
        try{
            byte a = Byte.parseByte(m);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    public ArrayList<Token> lex() throws LexingException{
        if(lexed){
            return list;
        }
        Pattern regex = Pattern.compile("(r)([0-4]\n?)|([a-z]+)\n?|(\\d+)\n?");
        Pattern splitter = Pattern.compile("\\s+|(//).*");
        int line = 0;

        for(String i : splitter.split(text)){
            if(i.equals("")){continue;}
            Matcher rmatch = regex.matcher(i);
            if(!rmatch.find()){
                throw new LexingException("Could not match token " + i, line);
            }
            if(i.contains("\n")){
                line++;
            }
            if(rmatch.group(0).startsWith("r") && rmatch.group(2) != null){
                try {
                    list.add(new TRegister(Byte.parseByte(rmatch.group(2))));
                }catch(Exception e){
                    throw new LexingException(i + " is not a valid token", line);
                }
            }else if(isByte(rmatch.group(0))){
                list.add(new TLiteral(Byte.parseByte(i)));
            }else{
                list.add(new TInstruction(i));
            }
        }

        lexed = true;

        //return cloned arraylist to prevent mutation
        return new ArrayList<>(list);
    }
}
