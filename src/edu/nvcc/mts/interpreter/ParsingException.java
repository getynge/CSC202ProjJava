package edu.nvcc.mts.interpreter;

public class ParsingException extends Exception {
    public final int line;
    ParsingException(String reason, int line){
        super(reason);
        this.line = line;
    }
}
