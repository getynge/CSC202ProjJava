package edu.nvcc.mts.interpreter;

public enum StatementType {
    Register,
    Memory,
    Value,
    DebugValue,
    None
}
