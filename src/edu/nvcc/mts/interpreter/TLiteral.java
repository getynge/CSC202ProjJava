package edu.nvcc.mts.interpreter;

public class TLiteral implements Token {
    final byte value;

    @Override
    public boolean innerMatch(Token other) {
        if(outerMatch(other)){
            TLiteral o = (TLiteral) other;
            return o.value == this.value;
        }
        return false;
    }

    TLiteral(byte value){
        this.value = value;
    }

    @Override
    public String toString() {
        return Byte.toString(value);
    }
}
