package edu.nvcc.mts.interpreter;

public class TRegister implements Token {
    final byte registerIndex;
    @Override
    public boolean innerMatch(Token other) {
        if(outerMatch(other)){
            TRegister o = (TRegister) other;
            return o.registerIndex == registerIndex;
        }
        return false;
    }

    TRegister(byte index){
        registerIndex = index;
    }

    @Override
    public String toString() {
        return "r" + Byte.toString(registerIndex);
    }
}
