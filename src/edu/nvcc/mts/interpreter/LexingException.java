package edu.nvcc.mts.interpreter;

public class LexingException extends Exception {
    public final int line;
    LexingException(String reason, int line){
        super(reason);
        this.line = line;
    }
}
