package edu.nvcc.mts.interpreter;

import edu.nvcc.mts.container.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Parser {

    private static final List<Pair<String, List<StatementType>>> sTable = Arrays.asList(
            new Pair<>("add", Arrays.asList(StatementType.Register, StatementType.Register)),
            new Pair<>("mult", Arrays.asList(StatementType.Register, StatementType.Register)),
            new Pair<>("store", Arrays.asList(StatementType.Register, StatementType.Memory)),
            new Pair<>("fetch", Arrays.asList(StatementType.Register, StatementType.Memory)),
            new Pair<>("equ", Arrays.asList(StatementType.Register, StatementType.Register, StatementType.Register)),
            new Pair<>("push", Collections.singletonList(StatementType.Register)),
            new Pair<>("pop", Collections.singletonList(StatementType.Register)),
            new Pair<>("goto", Collections.singletonList(StatementType.Register)),
            new Pair<>("load", Arrays.asList(StatementType.Register, StatementType.Value)),
            new Pair<>("out", Collections.singletonList(StatementType.Register)),
            new Pair<>("if", Arrays.asList(StatementType.Register, StatementType.Register)),
            new Pair<>("debug", Collections.singletonList(StatementType.DebugValue)),
            new Pair<>("end", Collections.emptyList())
    );

    private final ArrayList<Statement> list = new ArrayList<>();
    private final ArrayList<Token> input;
    private boolean done = false;

    public Parser(ArrayList<Token> inp){
        input = inp;
    }

    public List<Statement> parse() throws ParsingException{
        if(done){
            return Collections.unmodifiableList(list);
        }
        int next = 0;
        int nextIndex = -1;
        int line = 0;
        Statement toPush = new Statement();
        for(Token i : input){
            boolean notfound = true;
            if (next > 0){
                List<StatementType> table = sTable.get(nextIndex).second;
                StatementType expected = table.get(table.size() - next--);
                byte limit = Byte.MAX_VALUE;
                byte minimum = Byte.MIN_VALUE;
                switch(expected){
                    case Memory:
                    case DebugValue:
                    case Value:
                        if(expected == StatementType.Memory){
                            limit = 30;
                            minimum = 0;
                        }else if(expected == StatementType.DebugValue){
                            limit = 4;
                            minimum = 0;
                        }
                        if(!i.getClass().equals(TLiteral.class)){
                            throw new ParsingException("Expected literal value, got something else", line);
                        }
                        TLiteral literal = (TLiteral) i;
                        if(literal.value > limit){
                            throw new ParsingException("User supplied value for literal is out of valid range, attempted value was: " + literal.value + " limit is: " + limit, line);
                        }else if(literal.value < minimum){
                            throw new ParsingException("User supplied value for literal is out of valid range, attempted value was: " + literal.value + " minimum is: " + minimum, line);
                        }

                        toPush.addArgument(literal.value);
                        break;
                    case Register:
                        if(!i.getClass().equals(TRegister.class)){
                            throw new ParsingException("Expected register, got " + i.toString(), line);
                        }
                        TRegister register = (TRegister) i;
                        if(register.registerIndex > 4){
                            throw new ParsingException("User supplied value for register that is outside of valid range", line);
                        }
                        toPush.addArgument(register.registerIndex);
                        break;
                    default:
                        throw new UnreachableException();
                }

                notfound = false;

                if(next == 0){
                    list.add(toPush);
                    toPush = new Statement();
                    line++;
                }
            } else {
                for (int y = 0; y  < sTable.size(); y++) {
                    if (i.innerMatch(new TInstruction(sTable.get(y).first))) {
                        next = sTable.get(y).second.size();
                        toPush.opcode = sTable.get(y).first;
                        if(next > 0){
                            nextIndex = y;
                        }else{
                            list.add(toPush);
                            toPush = new Statement();
                            line++;
                        }
                        notfound = false;
                        break;
                    }
                }
            }

            if(notfound){
                throw new ParsingException("Unexpected Token: " + i.toString(), line);
            }
        }

        done = true;
        return Collections.unmodifiableList(list);
    }
}