package edu.nvcc.mts.interpreter;

class UnreachableException extends RuntimeException {
    UnreachableException(){
        super("Code that should be unreachable has been reached");
    }
}
