package edu.nvcc.mts.interpreter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Statement {
    public String opcode = "";
    private final ArrayList<Byte> list = new ArrayList<>();
    void addArgument(byte argument){
        list.add(argument);
    }

    public List<Byte> getArguments(){
        return Collections.unmodifiableList(list);
    }
}
