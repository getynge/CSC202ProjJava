package edu.nvcc.mts.interpreter;

public interface Token {
    default boolean outerMatch(Token other){
        return this.getClass().equals(other.getClass());
    }
    boolean innerMatch(Token other);
}
